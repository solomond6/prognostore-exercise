var progno = angular.module("prognoStore", []);

// var prognoCtrl = function($scope){
// 	$scope.title = "Prognostore Exercise";
// }

progno.controller("prognoCtrl", function($scope, $http){
	$scope.title = "Prognostore Exercise";
	$scope.lat = "9";
	$scope.lng = "7";
	var successCallBack = function(response){
		$scope.records = response.data;
		// console.log($scope.records);
	}
	var errorCallBack = function(response){
		$scope.error = response.data;
		// console.log(response.data);
	}
	$http({
		method:'GET',
		url:'http://api.geonames.org/findNearByWeatherJSON?lat='+$scope.lat+'&lng='+$scope.lng+'&username=prognotest',
		cache: true
	}).then(successCallBack,errorCallBack);

	$scope.change = function() {
        $http({
			method:'GET',
			url:'http://api.geonames.org/findNearByWeatherJSON?lat='+$scope.lat+'&lng='+$scope.lng+'&username=prognotest',
			cache: true
		}).then(successCallBack,errorCallBack);
    };
});